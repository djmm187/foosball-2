/*
 * This file launches the application by asking Ext JS to create
 * and launch() the Application class.
 */
Ext.application({
    extend: 'FOOSBALL.Application',

    name: 'FOOSBALL',

    requires: [
        // This will automatically load all classes in the FOOSBALL namespace
        // so that application classes do not need to require each other.
        'FOOSBALL.*'
    ],

    // The name of the initial view to create.
    mainView: 'FOOSBALL.view.main.Main'
});
