## Foosball 2.0
This was a coding challenge from 4 years ago. The app was originally written in ExtJS 4.2 and has been ported over to ExtJS 6.5.

The simple requirements were to give the user the ability to view foosball match details, i.e. player names and scores, and keep a "realtime" aggregated scoreboard of all foosball matches.

See original app: [https://github.com/djmm187/foosball](https://github.com/djmm187/foosball)

## Dependancies

 * Sencha CMD (6.5) - [https://www.sencha.com/products/extjs/cmd-download/](https://www.sencha.com/products/extjs/cmd-download/)
 * Sencha ExtJS (6.5) - [https://www.sencha.com/products/evaluate/](https://www.sencha.com/products/evaluate/)
 * Git - [https://git-scm.com/downloads](https://git-scm.com/downloads)

## Usage

Clone and navigate the repository:
```
git clone https://djmm187@bitbucket.org/djmm187/foosball-2.git
cd ./foosball-2
```

Navigate to the app and start up dev server:
```
sencha app watch classic
```

To compile a production build:
```
sencha app build
```

This will compile all of the Sass files into CSS and minify your applications javascript code. You can view the production application
by pointing your webserver at:
    `$(pwd)/build/production` 
