/**
 * @class FOOSBALL.ux.grid.Base
 * @extends Ext.grid.Panel
 * This is a base grid class that contains default grid configurations,
 * styling base class, and action column convenience methods.
 *
 * See the usage example below:
 *
 * @example
 *
 *      // view component
 *      Ext.define('My.Grid', {
 *          extends: 'FOOSBALL.ux.grid.Base',
 *
 *          controller: 'my.grid',
 *
 *          title: 'Foo',
 *
 *          columns: [
 *               {
 *                   text: 'Bar',
 *                   dataIndex: 'bar'
 *               }
 *           ],
 *
 *          initComponent: function() {
 *              // scope
 *               var me = this;
 *
 *               // allow the user to view a row
 *               me.allowedActions = ['view'];
 *
 *               // override
 *               me.callParent();
 *           }
 *      });
 *
 *       // view controller
 *       Ext.define('My.GridController', {
 *           extend: 'Ext.app.ViewController',
 *
 *           alias: 'controller.my.grid',
 *
 *           viewRecord: function(grid, rowIndex, colIndex) {
 *               var rec = grid.getStore().getAt(rowIndex);
 *
 *               // do some magic with our store record
 *           }
 *       });
 */
Ext.define('FOOSBALL.ux.grid.Base', {

    extend: 'Ext.grid.Panel',

    requires: [
        'Ext.grid.Panel'
    ],

    scrollable: true,

    viewConfig: {

        forceFit: true,

        markDirty: false
    },

    columns: [],

    cls: 'base-grid',

    rowActions: {

        'view': {
            iconCls: 'fa fa-eye',
            tooltip: 'View',
            itemId: 'view',
            handler: 'viewRecord'
        },

        'edit': {
            iconCls: 'fa fa-pencil',
            tooltip: 'Edit',
            itemId: 'edit',
            handler: 'editRecord'
        },

        'delete': {
            iconCls: 'fa fa-trash',
            tooltip: 'Delete',
            itemId: 'delete',
            handler: 'deleteRecord'
        },

        'clone': {
            iconCls: 'fa fa-files-o',
            tooltip: 'Clone',
            itemId: 'clone',
            handler: 'cloneRecord'
        }
    },

    allowedActions: [],

    addColumns: function(cols) {
        var me = this;

        me.columns = cols.concat(me.columns);

        return me;
    },

    initComponent: function() {
        var me = this,
            actions = [];

        if (me.allowedActions.length) {
            Ext.each(me.allowedActions, function(action) {
                if (me.rowActions.hasOwnProperty(action)) {
                    actions.push(me.rowActions[action]);
                }
            });

            me.columns.push({
                text: 'Actions',
                xtype: 'actioncolumn',
                items: actions,
                itemId: 'actions'
            });
        }

        me.callParent();
    }
});
