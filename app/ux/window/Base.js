/**
 * @class FOOSBALL.ux.window.Base
 */
Ext.define('FOOSBALL.ux.window.Base', {

    extend: 'Ext.window.Window',

    title: null,

    modal: true,

    closeable: true,

    closeAction: 'destory',

    maximized: false,

    autoShow: true,

    minWidth: '200',

    minHeight: '200',

    y: 0
});