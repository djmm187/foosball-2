/**
 *  @class FOOSBALL.store.Games
 */
Ext.define('FOOSBALL.store.Games',{
    extend: 'Ext.data.Store',
    model: 'FOOSBALL.model.Game',
    storeId: 'games',
    alias: 'store.games',
    proxy: {
        type: 'ajax',
        url: '/resources/data/scores.json',
        reader: {
            type: 'json'
        }
    },
    autoLoad: true
});