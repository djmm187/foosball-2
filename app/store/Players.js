/**
 *  @class FOOSBALL.store.Players
 */
Ext.define('FOOSBALL.store.Players',{
    extend: 'Ext.data.Store',
    model: 'FOOSBALL.model.Player',
    storeId: 'players',
    alias: 'store.players',
    proxy: {
        type: 'ajax',
        url: '/resources/data/players.json',
        reader: {
            type: 'json'
        }
    },
    autoLoad: true,

    sorters : {
        property : 'wins',
        direction : 'DESC'
    }
});