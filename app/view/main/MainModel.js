/**
 * @class FOOSBALL.view.main.MainModel
 */
Ext.define('FOOSBALL.view.main.MainModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.main',

    data: {
        name: 'Foosball Scoreboard'
    }
});
