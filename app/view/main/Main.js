/**
 * @class FOOSBALL.view.main.Main
 */
Ext.define('FOOSBALL.view.main.Main', {

    extend: 'Ext.panel.Panel',

    xtype: 'app-main',

    requires: [
        'Ext.*',
        'Ext.plugin.Viewport',

        'FOOSBALL.*'
    ],

    controller: 'main',

    viewModel: 'main',

    id: 'main',

    header: {
        margin: 0,
        cls: 'app-header',
        title: {
            bind: {
                text: '{name}'
            }
        }
    },

    defaults: {
        flex: 1
    },

    items: [
        {
            xtype: 'games'
        }
    ]
});
