/**
 * @class FOOSBALL.view.games.GamesModel
 */
Ext.define('FOOSBALL.view.games.GamesModel', {

    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.games',

    data: {
        titles: {
            results: 'Recent Games',
            scores: 'Player Scores',
            editor: 'Add a New Game'
        }
    },

    stores: {
    	scores: {
    		type: 'games',
            autoLoad: true
        },

        results: {
            type: 'players',
            autoLoad: true
        }
    }
});
