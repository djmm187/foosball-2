/**
 * @class FOOSBALL.view.games.GamesEditor
 */
Ext.define('FOOSBALL.view.games.GamesEditor', {
    extend: 'FOOSBALL.ux.window.Base',

    requires: [
        'FOOSBALL.view.games.GamesController',
        'FOOSBALL.view.games.GamesModel'
    ],

    controller: 'games',

    viewModel: {
        type: 'games'
    },

    itemId: 'gamesEditor',

    xtype: 'gamesEditor',

    bind: {
        title: '{titles.editor}'
    },

    cls: 'games-editor',

    initComponent: function () {
        var me = this;

        me.items = [
            {
                xtype: 'form',
                bodyPadding: 5,
                defaults: {
                    allowBlank: false,
                    xtype: 'numberfield',
                },
                items: [
                    {
                        fieldLabel: 'Player 1',
                        xtype: 'combobox',
                        name: 'player1',
                        valueField: 'name',
                        displayField: 'name',
                        bind: {
                            store: '{results}'
                        },
                        typeAhead: true,
                        queryMode: 'local'

                    },
                    {
                        fieldLabel: 'Player 1 Score',
                        name: 'score1'
                    },
                    {
                        fieldLabel: 'Player 2',
                        xtype: 'combobox',
                        name: 'player2',
                        valueField: 'name',
                        displayField: 'name',
                        bind: {
                            store: '{results}'
                        },
                        typeAhead: true,
                        queryMode: 'local'
                    },
                    {
                        fieldLabel: 'Player 2 Score',
                        name: 'score2'
                    }
                ],
                buttons: [
                    '->',
                    {
                        text: 'Save New Game',
                        itemId: 'saveForm',
                        handler: 'saveGame'
                    },
                    {
                        text: 'Cancel',
                        itemId: 'cancel',
                        handler: 'closeEditor'
                    }
                ]
            }
        ];

        me.callParent();
    }
});
