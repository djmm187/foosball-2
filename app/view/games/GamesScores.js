/**
 * @class FOOSBALL.view.games.GamesScores
 */
Ext.define('FOOSBALL.view.games.GamesScores', {
    extend: 'FOOSBALL.ux.grid.Base',

    requires: [
        'FOOSBALL.view.games.GamesController',
        'FOOSBALL.view.games.GamesModel',

        // views
        'FOOSBALL.view.games.GamesEditor',
        'FOOSBALL.view.games.GamesResults'
    ],

    controller: 'games',

    viewModel: {
        type: 'games'
    },

    itemId: 'scores',

    alias: 'widget.scores',

    bind: {
        store: '{scores}'
    },

    padding: 0,

    initComponent: function () {
        var me = this;

        me.dockedItems = [
            {
                xtype: 'toolbar',
                dock: 'top',
                border: 0,
                items: [
                    {
                        xtype: 'displayfield',
                        bind: {
                            value: '{titles.results}'
                        },
                    },
                    '->',
                    {
                        xtype: 'button',
                        text: 'Add Game',
                        itemId: 'addGame',
                        handler: 'addGame'
                    }
                ]
            }
        ];

        me.columns = [
            {
                text: 'Player 1',
                dataIndex: 'player1',
                flex: 2
            },
            {
                text: 'Score',
                dataIndex: 'score1',
                flex: 1
            },
            {
                text: 'Player 2',
                dataIndex: 'player2',
                flex: 2
            },
            {
                text: 'Score',
                dataIndex: 'score2',
                flex: 1
            }
        ];

        me.allowedActions = [];

        me.callParent();
    }
});
