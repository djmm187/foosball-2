Ext.define('FOOSBALL.view.games.GamesController', {

    extend: 'Ext.app.ViewController',

    alias: 'controller.games',

    // open a new game modal
    addGame: function (btn) {
        var me = this,
            grid = btn.up('panel'),
            form;

        return Ext.create('FOOSBALL.view.games.GamesEditor');
    },

    // close editor
    closeEditor: function (btn) {
        btn.up('window').close();
    },

    // save game data
    saveGame: function (btn) {
        var me = this,
            panel = btn.up('form'),
            form = panel.getForm(),
            values = form.getValues(),
            main = Ext.getCmp('main'),
            grid = main.down('scores'),
            resultsGrid = main.down('results');

        if (form.isValid()) {
            panel.setLoading('Saving new Game...');

            //add our new record
            grid.getStore().add(values);

            //update our score board
            me.updateScoreBoardRanks(resultsGrid.getStore(), values);

            //remove loader and close the window
            panel.setLoading(false);
            panel.up('window').close();
        }
    },

    // fetch game data
    afterGameStoreLoad: function(cmp) {
        var me = this,
        rankGrid = Ext.getCmp('results');

        // loader and populate our scoreboard
        rankGrid.setLoading(true);
        setTimeout(function(){
            me.populateRankings(rankGrid);
        }, 200);
    },

    // populate our ranking board
    populateRankings: function(cmp) {
        var me = this,
            vm = me.getViewModel(),
            gameScores = vm.get('scores'),
            results = vm.get('results'),
            rec;

        // tally up the init game results
        gameScores.each(function(item, index, count) {
            rec = item.raw;

            //no need to compare equal scores. no one can brag about a stalemate
            if (rec.score1 !== rec.score2) {
                me.updateScoreBoardRanks(results, rec);
            }
        });

        cmp.setLoading(false);
    },

    // update our scoreboard
    updateScoreBoardRanks: function (store, rec) {
        var winner,
            targetRec;

        // if its a tie, no one wins
        if (rec.score1 == rec.score2) {
            return;
        }

        // lets find the winner
        winner = (rec.score1 > rec.score2) ? rec.player1 : rec.player2;

        // find our winning player from our store
        targetRec = store.findRecord('name', winner);

        // update winning count
        targetRec.set('wins', parseInt(targetRec.get('wins')) + 1);
    },

    // remove record from store
    onRemoveClick: function (grid, rowIndex){
        this.getStore().removeAt(rowIndex);
    }
});
