/**
 * @class FOOSBALL.view.games.Games
 */
Ext.define('FOOSBALL.view.games.Games', {
    extend: 'Ext.panel.Panel',

    requires: [
        'FOOSBALL.view.games.GamesController',
        'FOOSBALL.view.games.GamesModel',

        // views
        'FOOSBALL.view.games.GamesEditor',
        'FOOSBALL.view.games.GamesResults',
        'FOOSBALL.view.games.GamesScores'
    ],

    controller: 'games',

    viewModel: {
        type: 'games'
    },

    itemId: 'games',

    alias: 'widget.games',

    cls: 'games',

    padding: 0,

    defaults: {
        flex: 1,
        border: 0,
        frame: false,
        margin: 10,
        height: 300,
        bodyPadding: 0,
        cls: 'games-grid',
        bodyStyle: 'background: #fff',
    },

    layout: 'hbox',

    items: [
        {
            xtype: 'scores',
            flex: 4
        },
        {
            xtype: 'results'
        }
    ]
});
