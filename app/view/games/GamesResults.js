/**
 *  @class FOOSBALL.view.games.GamesResults
 */
Ext.define('FOOSBALL.view.games.GamesResults', {

    extend: 'FOOSBALL.ux.grid.Base',

    requires: [
        'FOOSBALL.view.games.GamesController',
        'FOOSBALL.view.games.GamesModel'
    ],

    controller: 'games',
    viewModel: {
        type: 'games'
    },

    title: null,

    layout: 'fit',

    itemId: 'results',

    alias: 'widget.results',

    bind: {
        store: '{results}'
    },

    tbar:  [
        {
            xtype: 'displayfield',
            bind: {
                value: '{titles.scores}'
            }
        }
    ],

    columns: [
        {
            text: 'Player',
            dataIndex: 'name',
            flex: 1
        },
        {
            text: 'Wins',
            dataIndex: 'wins',
            flex: 1
        }
    ],

    allowedActions: []
});
