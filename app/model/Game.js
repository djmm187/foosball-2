/**
 *  @class FOOSBALL.model.Game
 */
Ext.define('FOOSBALL.model.Game', {
	extend: 'FOOSBALL.model.Base',
	fields: [
		'player1',
		'player2',
		'score1',
		'score2'
	]
});