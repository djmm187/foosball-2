/**
 * @class 'FOOSBALL.model.Base'
 */
Ext.define('FOOSBALL.model.Base', {
    extend: 'Ext.data.Model',

    schema: {
        namespace: 'FOOSBALL.model'
    }
});
