/**
*  @class FOOSBALL.model.Player
*/
Ext.define('FOOSBALL.model.Player', {
	extend: 'FOOSBALL.model.Base',
	fields: [
		'name',
		'wins'
	]
});